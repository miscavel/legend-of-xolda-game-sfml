# Legend of Xolda Game SFML

Legend of Xolda Game made in SFML-2.3.2

Project compiled using Visual Studio 2010 32-bit

A game that aspires to incorporate Zelda's gameplay into Ragnarok Online.

Controls :
1. Movement - Arrow keys
2. Attack - 'Z' key
3. Respawn - 'R' key (only available upon death)
4. Status Menu - 'K' key
5. Quit - 'Escape' key

Gameplay :
1. By defeating the enemies, player may accumulate experience points, and levels up once the requirement is met.
2. Leveling up earns the player 2 status points, which can be allocated to the 4 status available : HP, Attack, Def, or MP (as of this release, MP is not used for anything).
3. Player may save the level and status gained through the status menu.
4. After defeating all the enemies on the map, the enemies will respawn. The enemies' strength and type are determined by the player's level.
5. Player's goal is to defeat the Valkyrie that may spawn randomly at higher levels.

Note : This was one of my earliest projects using C++, and has inefficient use of pointers thereby causing the low FPS.

![Gameplay Video](Media/trailer.mp4)

Credits to :
1. Gravity Co., Ltd for the sprites, BGMs, and sound effects.
2. Square Enix for the title BGM.