#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

class Object : public sf::Sprite
{
	protected :
		int img_index ;
		Sprite wall_sprite;
	public :
		Object()
		{
			img_index = 0;
			wall_sprite.setTextureRect(sf::IntRect(img_index*64,64*11,52,57));
			wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 3 );
		}
		Object(const sf::Texture &_texture, const sf::Sprite &_sprite , int _img_index = 0) : sf::Sprite( _texture )
		{
			wall_sprite = _sprite;
			img_index = _img_index;
		}
		void setWallSprite(const sf::Sprite &_sprite)
		{
			wall_sprite = _sprite;
		}
};
class Living_Object : public Object
{
protected :
	bool check_left, check_right, check_up, check_down;
	bool check_atk;
	int delay , delay_atk;
	int mdelay , mdelay_atk;
	int speed;
	int move_img;
	int atk_img_index;
public :
	Living_Object ()
	{
		check_right = true;
		check_left = false;
		check_up = false;
		check_down = false;
		delay = 3;
		mdelay = 3;
		delay_atk = 20;
		mdelay_atk = 20;
		speed = 2;
		move_img = 12;
		atk_img_index = 0;
	}
	Living_Object(int _delay , int _delay_atk , int _speed , int _move_img)
	{
		check_right = true;
		check_left = false;
		check_up = false;
		check_down = false;
		delay = _delay;
		mdelay = _delay;
		delay_atk = delay_atk;
		mdelay_atk = delay_atk;
		speed = _speed;
		move_img = _move_img;
		atk_img_index = 0;
	}
	void setCheckAtk( bool _check_atk )
	{
		check_atk = _check_atk;
	}
	bool getCheckAtk()
	{
		return check_atk;
	}
	void Atk_Animation()
	{
		for ( int i = 0 ; i < 100 ; i++)
		{
			if ( delay_atk > 0 )
			{
				delay_atk--;
			}
			else
			{
				delay_atk = mdelay_atk;
				atk_img_index ++;
			}
			if ( check_down == true )
			{
				setTextureRect(sf::IntRect(atk_img_index*64,64*14,64,64));
			}
			if ( check_up == true )
			{
				setTextureRect(sf::IntRect(atk_img_index*64,64*12,64,64));
			}
			if ( check_left == true )
			{
				setTextureRect(sf::IntRect(atk_img_index*64,64*13,64,64));
			}
			if ( check_right == true )
			{
				setTextureRect(sf::IntRect(atk_img_index*64,64*15,64,64));
			}
			if ( atk_img_index >= 5 )
			{
				atk_img_index = 0;
			}
		}
	}
	void Move_Animation(vector<sf::Sprite> wall_object , int index)
	{
		if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			if ( delay > 0 )
			{
				delay --;
			}
			else
			{
				img_index ++;
				delay = 5;
			}
			if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				if ( check_left == false && check_down == false && check_up == false )
				{
					check_right = true;
				}
				if ( check_right == true )
				{
					move_img = 12;
				}
				if ( check_left == false  )
				{
					setPosition(getPosition().x + speed,getPosition().y);
					wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 3 );
					if ( CheckHitBox(wall_sprite,wall_object,index) == true )
					{
						setPosition(getPosition().x - speed,getPosition().y);
						wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 4 );
					}
				}
			}
			else
			{
				check_right = false;
			}
			if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				if ( check_right == false && check_down == false && check_up == false )
				{
					check_left = true;
				}
				if ( check_left == true )
				{
					move_img = 10;
				}
				if ( check_right == false )
				{					
					setPosition(getPosition().x - speed,getPosition().y);
					wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 3 );
					if ( CheckHitBox(wall_sprite,wall_object,index) == true )
					{
						setPosition(getPosition().x + speed,getPosition().y);
						wall_sprite.setPosition( getPosition().x + 4 , getPosition().y + 3 );
					}
				}
			}
			else
			{
				check_left = false;
			}
			if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				if ( check_right == false && check_down == false && check_left == false )
				{
					check_up = true;
				}
				if ( check_up == true )
				{
					move_img = 9;
				}
				if ( check_down == false )
				{				
					setPosition(getPosition().x,getPosition().y - speed);
					wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 3 );
					if ( CheckHitBox(wall_sprite,wall_object,index) == true )
					{
						setPosition(getPosition().x,getPosition().y + speed);
						wall_sprite.setPosition(getPosition().x + 5 , getPosition().y + 3);
					}
				}
			}
			else
			{
				check_up = false;
			}
			if( sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				if ( check_right == false && check_left == false && check_up == false )
				{
					check_down = true;
				}
				if ( check_down == true )
				{
					move_img = 11;
				}
				if ( check_up == false )
				{				
					setPosition(getPosition().x,getPosition().y + speed);
					wall_sprite.setPosition(getPosition().x + 5 , getPosition().y + 3 );
					if ( CheckHitBox(wall_sprite,wall_object,index) == true )
					{
						setPosition(getPosition().x,getPosition().y - speed);
						wall_sprite.setPosition(getPosition().x + 5 , getPosition().y + 3 );
					}
				}
			}
			else
			{
				check_down = false;
			}
		}
		else
		{
			img_index = 0;
		}
		if ( img_index >= 8 )
		{
			img_index = 0;
		}
		if ( check_atk == false )
		{
			setTextureRect(sf::IntRect(img_index*64,64*(move_img-1),64,64));
		}
		setTextureRect(sf::IntRect(img_index*64,64*(move_img-1),64,64));
		wall_sprite.setTextureRect(sf::IntRect(img_index*64,64*(move_img-1),57,57));
	}
	bool CheckHitBox (sf::Sprite _target, vector<sf::Sprite>  _wall, int size)
	{
		for ( int i = 0 ;  i < size ; i ++ )
		{
			if (_target.getGlobalBounds().intersects(_wall[i].getGlobalBounds()))
			{
				return true;
			}
			if ( i == size - 1 )
			{
				return false;
			}
		}
	}
};


void main()
{
	sf::RenderWindow window(sf::VideoMode(1000,700), "Xelda"); // buat window
	string coba;
	ifstream infile;
	infile.open("coba.txt");
	sf::Texture texture_object;
	vector <Object>sprite_object(1000);
	vector <sf::Sprite>wall_object(1000);
	int index = 0;
	if ( infile.is_open() )
	{
		double j = 2.0;
		while ( infile >> coba)
		{
			for ( int i = 0 ; i < coba.length() ; i++)
			{
				if ( coba[i] == '*' )
				{
					texture_object.loadFromFile("boulder.png",sf::IntRect(0,0,32,32));
					wall_object[index].setTextureRect(sf::IntRect(0,0,1,1));
					wall_object[index].setPosition(i * 32 + 17 , j - 3);
					sprite_object[index] = Object( texture_object , wall_object[index] , 0 );
					sprite_object[index].setPosition(i * 32 , j );
					index++;
				}
			}
			j += 30;
		}
	}
	infile.close();
	sf::Texture texture;
	texture.loadFromFile("download.png");
	Living_Object sprite;
	sprite.setTexture(texture);
	sprite.setPosition(50.0,255.0);
	sprite.setTextureRect(sf::IntRect(0*64,64*11,64,64));
	while ( window.isOpen() )
	{
		sf::Event event_coba; 
		while ( window.pollEvent(event_coba) ) 
		{
			if ( event_coba.type == sf::Event::Closed )
			{
				window.close();
			}
		}
		window.clear(); 
		sprite.setTexture(texture);
		sprite.Move_Animation(wall_object , index);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			    sprite.Atk_Animation();
				for ( int j = 0 ; j < index ; j++)
				{
					window.draw(sprite_object[j]);
				}
				window.draw(sprite);
				window.display();
				window.clear();
				sprite.setCheckAtk(true);
		}
		else
		{
			sprite.setCheckAtk(false);
		}
		for ( int i = 0 ; i < index ; i ++)
		{
			window.draw(sprite_object[i]);
		}
		window.draw(sprite);
		window.display();
		window.setFramerateLimit(90);
	}
}
