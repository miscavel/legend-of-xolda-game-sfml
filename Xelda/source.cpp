#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <SFML/Audio.hpp>
#include <conio.h>

using namespace std;

bool CheckHitBox (sf::Sprite _target , vector<sf::Sprite>  _wall, int size)
{
	for ( int i = 0 ;  i < size ; i ++ )
	{
		if (_target.getGlobalBounds().intersects(_wall[i].getGlobalBounds()))
		{
			return true;
		}
		if ( i == size - 1 )
		{
			return false;
		}
	}
}

bool Battle (sf::Sprite attacker, sf::Sprite defender)
{
	if( attacker.getGlobalBounds().intersects(defender.getGlobalBounds()))
	{
		return true;
	}
	return false;
}

class Object : public sf::Sprite
{
	protected :
		Sprite wall_sprite;
	public :
		Object()
		{
			wall_sprite.setTextureRect(sf::IntRect(0*64,64*11,52,57));
			wall_sprite.setPosition( getPosition().x + 5 , getPosition().y + 3 );
		}
		Object(const sf::Texture &_texture, const sf::Sprite &_sprite ) : sf::Sprite( _texture )
		{
			wall_sprite = _sprite;
		}
		void setWallSprite(const sf::Sprite &_sprite)
		{
			wall_sprite = _sprite;
		}
};

class Sound_Wav
{
public :
	sf::SoundBuffer move_sound_buffer, attack_sound_buffer, hurt_sound_buffer, dead_sound_buffer;
	sf::Sound move_sound, attack_sound, hurt_sound, dead_sound;
	Sound_Wav()
	{
	}
	Sound_Wav(std::string move_name, std::string attack_name, std::string hurt_name, std::string dead_name)
	{
		move_sound_buffer.loadFromFile(move_name);
		attack_sound_buffer.loadFromFile(attack_name);
		hurt_sound_buffer.loadFromFile(hurt_name);
		dead_sound_buffer.loadFromFile(dead_name);

		move_sound.setBuffer(move_sound_buffer);
		attack_sound.setBuffer(attack_sound_buffer);
		hurt_sound.setBuffer(hurt_sound_buffer);
		dead_sound.setBuffer(dead_sound_buffer);

		move_sound.setVolume(30);
		attack_sound.setVolume(30);
		hurt_sound.setVolume(30);
		dead_sound.setVolume(30);
	}
	void Set_Wav(std::string move_name, std::string attack_name, std::string hurt_name, std::string dead_name)
	{
		move_sound_buffer.loadFromFile(move_name);
		attack_sound_buffer.loadFromFile(attack_name);
		hurt_sound_buffer.loadFromFile(hurt_name);
		dead_sound_buffer.loadFromFile(dead_name);

		move_sound.setBuffer(move_sound_buffer);
		attack_sound.setBuffer(attack_sound_buffer);
		hurt_sound.setBuffer(hurt_sound_buffer);
		dead_sound.setBuffer(dead_sound_buffer);

		move_sound.setVolume(30);
		attack_sound.setVolume(30);
		hurt_sound.setVolume(30);
		dead_sound.setVolume(30);
	}
};

class Living_Object
{
protected :
	int hp, mhp, mp, mmp, atk, def;
	bool check_left, check_right, check_up, check_down;
	bool dleft , dright , dup , ddown;
	bool check_atk, check_atked, dying, alive;
	bool aggro;
	int delay , delay_atk, delay_ori;
	int delay_hurt, delay_hurt_ori;
	int mdelay_atk;
	float speed;
	int atk_img_index;
	int img_index;
	int hurt_img_index;
	int die_img_index;
	int movement_direction;
	int direction_image;
	int movement_cycle;
	int movement_limit;
	sf::Sprite body;
	sf::Texture body_texture;
	sf::Sprite head;
	sf::Texture head_texture;
	sf::Sprite wall;
	sf::Texture weapon_texture;
	sf::Sprite weapon;
	sf::Texture weapon_aura_texture;
	sf::Sprite weapon_aura;
	int move_limit, atk_limit, hurt_limit, death_limit, idle_limit;
	int death_move_x, death_move_y;
	int id, aggresive, death_speed;
	int level, exp, mexp, skill_point;
	int attack_range_x, attack_range_y;
public :
	Living_Object ()
	{
		level = 1;
		exp = 0;
		hp = 10;
		mhp = hp;
		mp = 10;
		mmp = mp;
		atk = 3;
		def = 1;
		ddown = true;
		check_right = true;
		check_left = false;
		check_up = false;
		check_down = false;
		delay = 4;
		delay_atk = 4;
		mdelay_atk = delay_atk;
		speed = 2;
		atk_img_index = 0;
		img_index = 0;
		movement_direction = -1;
		movement_cycle = 0;
		direction_image = 0;
		movement_limit = 1;
		delay_ori = delay;
		check_atk = false;
		check_atked = false;
		delay_hurt = 4;
		delay_hurt_ori = delay_hurt;
		hurt_img_index = 0;
		die_img_index = 0;
		alive = true;
		dying = false;
		aggro = false;
	}
	Living_Object(int _delay , int _delay_atk , float _speed,int _idle_limit, int _move_limit,int _atk_limit,int _hurt_limit,int _death_limit, int _delay_hurt, int _hp, int _mp, int _atk, int _def, int _id, int _aggresive, int _death_move_x, int _death_move_y, int _death_speed, int _exp, int _attack_range_x, int _attack_range_y)
	{
		id = _id;
		hp = _hp;
		mhp = hp;
		mp = _mp;
		mmp = mp;
		atk = _atk;
		def = _def;
		ddown = true;
		check_right = true;
		check_left = false;
		check_up = false;
		check_down = false;
		delay = _delay;
		delay_atk = _delay_atk;
		mdelay_atk = delay_atk;
		speed = _speed;
		atk_img_index = 0;
		img_index = 0;
		movement_direction = -1;
		movement_cycle = 0;
		direction_image = 0;
		movement_limit = 1;
		delay_ori = delay;
		check_atk = false;
		check_atked = false;
		delay_hurt = 6;
		delay_hurt_ori = delay_hurt;
		hurt_img_index = 0;
		die_img_index = 0;
		alive = true;
		dying = false;
		idle_limit = _idle_limit;
		move_limit = _move_limit;
		atk_limit = _atk_limit;
		hurt_limit = _hurt_limit;
		death_limit = _death_limit;
		death_move_x = _death_move_x;
		death_move_y = _death_move_y;
		aggresive = _aggresive;
		if(aggresive == 1)
		{
			aggro = true;
		}
		else
		{
			aggro = false;
		}
		death_speed = _death_speed;
		exp = _exp;
		attack_range_x = _attack_range_x;
		attack_range_y = _attack_range_y;
		
		ostringstream convert_id;
		convert_id.str("");
		convert_id << id;
	}
	void setAlive(bool _alive)
	{
		alive = _alive;
	}
	void setCheckAtk( bool _check_atk )
	{
		check_atk = _check_atk;
		if(_check_atk == false)
		{
			atk_img_index = 0;
		}
	}
	void setCheckAtked( bool _check_atked)
	{
		check_atked = _check_atked;

		if(_check_atked == true)
		{
			movement_direction = -1;
		}
		
		if(_check_atked == false)
		{
			hurt_img_index = 0;
		}
	}
	bool getCheckAtk()
	{
		return check_atk;
	}
	bool getCheckAtked()
	{
		return check_atked;
	}
	bool getAlive()
	{
		return alive;
	}
	bool getDying()
	{
		return dying;
	}
	void setLevel(int _level)
	{
		level = _level;
	}
	int getExp()
	{
		return exp;
	}
	int getMExp()
	{
		return mexp;
	}
	int getLevel()
	{
		return level;
	}
	int getSkillPoint()
	{
		return skill_point;
	}
	void Update_Exp(int _exp)
	{
		exp = exp + _exp;
		while(exp >= mexp)
		{
			exp = exp - mexp;
			level += 1;
			skill_point += 1;
			Update_MExp();
		}

		if(exp < 0)
		{
			exp = 0;
		}
	}
	void Update_MExp()
	{
		mexp = level * 100;
	}
	void Random_Movement()
	{
		int move;
		if(movement_direction == -1 && aggro == false)
		{
			move = rand()%100;
			if(move == 99)
			{
				movement_direction = rand()%7;
				movement_cycle = 0;
			}
		}
	}
	virtual void Atk_Animation(vector<Living_Object*> player, vector<Sound_Wav> &wav_db)
	{
		string file_name;
		ostringstream convert_id;
		file_name.clear();
		convert_id.str("");
		convert_id << id;

		file_name = "monster/";
		file_name += convert_id.str();

		if ( delay_atk > 0)
		{
			delay_atk --;
		}
		else
		{
			atk_img_index ++;
			delay_atk = mdelay_atk;
		}

		if ( ddown == true )
		{
			file_name = file_name + "MATTACK1.png";
		}
		if ( dup == true )
		{
			file_name = file_name + "MATTACK5.png";
		}
		if ( dleft == true )
		{
			file_name = file_name + "MATTACK3.png";
		}
		if ( dright == true )
		{
			file_name = file_name + "MATTACK7.png";
		}

		body_texture.loadFromFile(file_name);

		if ( atk_img_index >= atk_limit )
		{
			atk_img_index = 0;
			check_atk = false;
		}
		body.setTexture(body_texture);
		body.setTextureRect(sf::IntRect(atk_img_index*body_texture.getSize().x/atk_limit,0,body_texture.getSize().x/atk_limit,body_texture.getSize().y));

		if ( atk_img_index == (atk_limit / 3) )
		{
			wav_db[id-1].attack_sound.play();
		}

		if ( atk_img_index == (atk_limit / 2) )
		{
			for(int i = 0; i < player.size(); i++)
			{
				if(Battle(body,player[i]->getWall()) == true)
				{
					if(player[i]->getCheckAtked() == false && player[i]->getDying() == false)
					{
						int damage;
						if(atk < player[i]->getDef())
						{
							damage = 0;
						}
						else
						{
							damage = atk - player[i]->getDef();
						}
						player[i]->setCheckAtked(true);
						player[i]->setHP(player[i]->getHP()-damage);
					}
				}
				else
				{
					player[i]->setCheckAtked(false);
				}
			}
		}
	}
	virtual void Death_Animation(sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Living_Object*> player, vector<Sound_Wav> &wav_db)
	{
		string file_name;
		ostringstream convert_id;
		file_name.clear();
		convert_id.str("");
		convert_id << id;

		file_name = "monster/";
		file_name += convert_id.str();

		if ( ddown == true )
		{
			file_name = file_name + "MDEAD1.png";
		}
		if ( dup == true )
		{
			file_name = file_name + "MDEAD5.png";
		}
		if ( dleft == true )
		{
			file_name = file_name + "MDEAD3.png";
		}
		if ( dright == true )
		{
			file_name = file_name + "MDEAD7.png";
		}
		body_texture.loadFromFile(file_name);
		body.setTexture(body_texture);
		body.setTextureRect(sf::IntRect(die_img_index*body_texture.getSize().x/death_limit,0,body_texture.getSize().x/death_limit,body_texture.getSize().y));
		/*while(die_img_index < 17)
		{
			body.setTextureRect(sf::IntRect(die_img_index*body_texture.getSize().x/17,0,body_texture.getSize().x/17,body_texture.getSize().y));
			for ( int i = 0 ; i < sprite_object.size() ; i ++)
			{
				window.draw(sprite_object[i]);
			}

			for(int i = 0; i < monster.size(); i++)
			{
				if(monster[i] -> getAlive() == true && monster[i] -> getDying() == false)
				{
					window.draw(monster[i] -> getBody());
				}
			}
			window.draw(player[0] -> getBody());
			window.draw(player[0] -> getHead());
			if(player[0] -> getCheckAtk() == true)
			{
				window.draw(player[0] -> getWeaponAura());
				window.draw(player[0] -> getWeapon());
			}
			window.draw(body);
			window.display();
			window.clear(); 
			die_img_index = die_img_index + 1;
		}*/
		if(die_img_index >= death_limit)
		{
			dying = false;
			alive = false;
			player[0] -> Update_Exp(exp);
		}
		else
		{
			die_img_index = die_img_index + death_speed;
		}
		
		/*alive = false;*/
	}
	virtual void Hurt_Animation(sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Living_Object*> player, vector<Sound_Wav> &wav_db)
	{
		string file_name;
		ostringstream convert_id;
		file_name.clear();
		convert_id.str("");
		convert_id << id;

		file_name = "monster/";
		file_name += convert_id.str();

		if ( delay_hurt > 0 )
		{
			delay_hurt --;
		}
		else
		{
			hurt_img_index ++;
			delay_hurt = delay_hurt_ori;
		}

		if(hp <= 0)
		{
			wav_db[id-1].hurt_sound.play();
			wav_db[id-1].dead_sound.play();
			dying = true;
			body.setPosition(body.getPosition().x + death_move_x, body.getPosition().y + death_move_y);
			/*Death_Animation(window,monster,sprite_object,player);*/
		}
		else
		{
			if ( ddown == true )
			{
				file_name = file_name + "MHURT1.png";
			}
			if ( dup == true )
			{
				file_name = file_name + "MHURT5.png";
			}
			if ( dleft == true )
			{
				file_name = file_name + "MHURT3.png";
			}
			if ( dright == true )
			{
				file_name = file_name + "MHURT7.png";
			}

			body_texture.loadFromFile(file_name);

			if ( hurt_img_index == 0 )
			{
				wav_db[id-1].hurt_sound.play();
			}

			if ( hurt_img_index >= hurt_limit )
			{
				hurt_img_index = 0;
				check_atked = false;
			}
			body.setTexture(body_texture);
			body.setTextureRect(sf::IntRect(hurt_img_index*body_texture.getSize().x/hurt_limit,0,body_texture.getSize().x/hurt_limit,body_texture.getSize().y));
		}
	}
	virtual void Move_Animation(vector<sf::Sprite> wall_object , int index, vector<Living_Object*> player, sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Sound_Wav> &wav_db)
	{
		string file_name;
		ostringstream convert_id;
		file_name.clear();
		convert_id.str("");
		convert_id << id;

		file_name = "monster/";
		file_name += convert_id.str();

		if ( check_atked == false && dying == false )
		{
			if ( aggro == true )
			{
				if(player[0] -> getAlive() == true)
				{
					if(abs(player[0] -> getBody().getPosition().x - body.getPosition().x) <= attack_range_x && abs(player[0] -> getBody().getPosition().y - (body.getPosition().y - 40)) <= attack_range_y)
					{
						movement_direction = -1;
						check_atk = true;
					}
					else if(abs(player[0] -> getBody().getPosition().x - body.getPosition().x) <= attack_range_x && abs(player[0] -> getBody().getPosition().y - (body.getPosition().y + 20)) <= attack_range_y)
					{
						movement_direction = -1;
						check_atk = true;
					}
					else if(player[0] -> getBody().getPosition().x > body.getPosition().x)
					{
						if(player[0] -> getBody().getPosition().y == body.getPosition().y - 40)
						{
							movement_direction = 6;
						}
						else if(player[0] -> getBody().getPosition().y < body.getPosition().y - 40)
						{
							movement_direction = 5;
						}
						else if(player[0] -> getBody().getPosition().y > body.getPosition().y - 40)
						{
							movement_direction = 7;
						}
					}
					else if(player[0] -> getBody().getPosition().x < body.getPosition().x)
					{
						if(player[0] -> getBody().getPosition().y == body.getPosition().y - 40)
						{
							movement_direction = 2;
						}
						else if(player[0] -> getBody().getPosition().y < body.getPosition().y - 40)
						{
							movement_direction = 3;
						}
						else if(player[0] -> getBody().getPosition().y > body.getPosition().y - 40)
						{
							movement_direction = 1;
						}
					}
					else if(player[0] -> getBody().getPosition().x == body.getPosition().x)
					{
						if(player[0] -> getBody().getPosition().y == body.getPosition().y - 40)
						{
							movement_direction = -1;
						}
						else if(player[0] -> getBody().getPosition().y < body.getPosition().y - 40)
						{
							movement_direction = 4;
						}
						else if(player[0] -> getBody().getPosition().y > body.getPosition().y - 40)
						{
							movement_direction = 0;
						}
					}
				}
				else
				{
					aggro = false;
				}
			}

			if (check_atk == false)
			{
				if ( delay > 0 )
				{
					delay --;
				}
				else
				{
					img_index ++;
					delay = delay_ori;
				}

				if (movement_direction != -1)
				{
					if (movement_direction == 0)
					{
						direction_image = 1;
						body.setPosition(body.getPosition().x,body.getPosition().y + speed);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x,body.getPosition().y - speed);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						}
					}
					else if (movement_direction == 1)
					{
						direction_image = 1;
						body.setPosition(body.getPosition().x - (speed / 2),body.getPosition().y + (speed / 2));
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x + (speed / 2),body.getPosition().y - (speed / 2));
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						}
					}
					else if (movement_direction == 2)
					{
						direction_image = 3;
						body.setPosition(body.getPosition().x - speed,body.getPosition().y);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x + speed,body.getPosition().y);
							wall.setPosition(body.getPosition().x + 4 , body.getPosition().y + 3 );
						}
					}
					else if (movement_direction == 3)
					{
						direction_image = 3;
						body.setPosition(body.getPosition().x - (speed / 2),body.getPosition().y - (speed / 2));
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x + (speed / 2),body.getPosition().y + (speed / 2));
							wall.setPosition(body.getPosition().x + 4 , body.getPosition().y + 3 );
						}
					}
					else if (movement_direction == 4)
					{
						direction_image = 5;
						body.setPosition(body.getPosition().x,body.getPosition().y - speed);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x,body.getPosition().y + speed);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3);
						}
					}
					else if (movement_direction == 5)
					{
						direction_image = 5;
						body.setPosition(body.getPosition().x + (speed / 2),body.getPosition().y - (speed / 2));
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x - (speed / 2),body.getPosition().y + (speed / 2));
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3);
						}
					}
					else if (movement_direction == 6)
					{
						direction_image = 7;
						body.setPosition(body.getPosition().x + speed,body.getPosition().y);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x - speed,body.getPosition().y);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 4 );
						}
					}
					else if (movement_direction == 7)
					{
						direction_image = 7;
						body.setPosition(body.getPosition().x + (speed / 2),body.getPosition().y + (speed / 2));
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x - (speed / 2),body.getPosition().y - (speed / 2));
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 4 );
						}
					}
			
					if(direction_image == 1)
					{
						dright = false;
						dleft = false;
						dup = false;
						ddown = true;
						file_name = file_name + "MMOVE1.png";
					}
					else if(direction_image == 3)
					{
						dright = false;
						dleft = true;
						dup = false;
						ddown = false;
						file_name = file_name + "MMOVE3.png";
					}
					else if(direction_image == 5)
					{
						dright = false;
						dleft = false;
						dup = true;
						ddown = false;
						file_name = file_name + "MMOVE5.png";
					}
					else if(direction_image == 7)
					{
						dright = true;
						dleft = false;
						dup = false;
						ddown = false;
						file_name = file_name + "MMOVE7.png";
					}

					body_texture.loadFromFile(file_name);

					if ( img_index >= move_limit )
					{
						img_index = 0;
						if(movement_cycle < movement_limit)
						{
							movement_cycle ++;
						}
						else
						{
							movement_direction = -1;
						}
						wav_db[id-1].move_sound.play();
					}
					body.setTexture(body_texture);
					body.setTextureRect(sf::IntRect(img_index*body_texture.getSize().x/move_limit,0,body_texture.getSize().x/move_limit,body_texture.getSize().y));
				}
				else
				{
					if ( ddown == true )
					{
						file_name = file_name + "MIDLE1.png";
					}
					else if ( dup == true )
					{
						file_name = file_name + "MIDLE5.png";
					}
					else if ( dleft == true )
					{
						file_name = file_name + "MIDLE3.png";
					}
					else if ( dright == true )
					{
						file_name = file_name + "MIDLE7.png";
					}
					body_texture.loadFromFile(file_name);

					if ( img_index >= idle_limit )
					{
						img_index = 0;
					}
					body.setTexture(body_texture);
					body.setTextureRect(sf::IntRect(img_index*body_texture.getSize().x/idle_limit,0,body_texture.getSize().x/idle_limit,body_texture.getSize().y));
				}
			}
		}
		
		if( dying == true )
		{
			Death_Animation(window,monster,sprite_object,player,wav_db);
		}
		else if( check_atked == true )
		{
			Hurt_Animation(window,monster,sprite_object,player,wav_db);
		}
		else if( check_atk == true )
		{
			Atk_Animation(player,wav_db);
		}
	}
	void setHead(sf::Sprite _head )
	{
		head = _head;
		head.setPosition(body.getPosition().x + 5 , body.getPosition().y - 17);
	}
	void setBody(sf::Sprite _body )
	{
		body = _body;
		wall.setTextureRect(body.getTextureRect());
		wall.setPosition(body.getPosition());
	}

	void setHeadPosition( int pos_x , int pos_y )
	{
		head.setPosition(pos_x , pos_y);
	}
	void setBodyPosition( int pos_x , int pos_y )
	{
		body.setPosition(pos_x , pos_y);
	}
	void setBodyTexture(sf::Texture _body_texture)
	{
		body_texture = _body_texture;
	}
	void setHeadTexture(sf::Texture _head_texture)
	{
		head_texture = _head_texture;
	}

	void setSkillPoint(int _skill_point)
	{
		skill_point = _skill_point;
	}
	void setHP(int _hp)
	{
		hp = _hp;
		if(hp < 0)
		{
			hp = 0;
		}
	}
	void setMHP(int _mhp)
	{
		mhp = _mhp;
	}
	void setMP(int _mp)
	{
		mp = _mp;
	}
	void setMMP(int _mmp)
	{
		mmp = _mmp;
	}
	void setAtk(int _atk)
	{
		atk = _atk;
	}
	void setDef(int _def)
	{
		def = _def;
	}
	void setAggro(bool _aggro)
	{
		aggro = _aggro;
	}

	void hpUP()
	{
		if(skill_point > 0)
		{
			mhp ++;
			hp = mhp;
			skill_point --;
		}
	}

	void mpUP()
	{
		if(skill_point > 0)
		{
			mmp ++;
			mp = mmp;
			skill_point --;
		}
	}

	void atkUP()
	{
		if(skill_point > 0)
		{
			atk ++;
			skill_point --;
		}
	}

	void defUP()
	{
		if(skill_point > 0)
		{
			def ++;
			skill_point --;
		}
	}

	int getHP()
	{
		return hp;
	}
	int getMHP()
	{
		return mhp;
	}
	int getMP()
	{
		return mp;
	}
	int getMMP()
	{
		return mmp;
	}
	int getAtk()
	{
		return atk;
	}
	int getDef()
	{
		return def;
	}
	int getIdleLimit()
	{
		return idle_limit;
	}
	int getID()
	{
		return id;
	}

	bool getAggro()
	{
		return aggro;
	}

	sf::Sprite getHead()
	{
		return head;
	}
	sf::Sprite getBody()
	{
		return body;
	}
	sf::Sprite getWeapon()
	{
		return weapon;
	}
	sf::Sprite getWeaponAura()
	{
		return weapon_aura;
	}
	sf::Sprite getWall()
	{
		return wall;
	}
};

class Character : public Living_Object
{
protected :
	float posx , posy;
	float posx_w, posy_w;
	float posx_wa, posy_wa;
	float xhead;
	float x;
	sf::SoundBuffer attack_hit_buffer, attack_sound_buffer, hurt_sound_buffer;
public :
	sf::Sound attack_hit, attack_sound, hurt_sound;
	Character()
	{
		x = 45;
		xhead = 35;
		posx = 5;
		posy = -17;
		skill_point = 0;
		attack_hit_buffer.loadFromFile("character/swordsman/1ATTACKHIT.wav");
		attack_hit.setBuffer(attack_hit_buffer);
		attack_sound_buffer.loadFromFile("character/swordsman/1ATTACK.wav");
		attack_sound.setBuffer(attack_sound_buffer);
		hurt_sound_buffer.loadFromFile("character/swordsman/1HURT.wav");
		hurt_sound.setBuffer(hurt_sound_buffer);

		attack_hit.setVolume(30);
		attack_sound.setVolume(30);
		hurt_sound.setVolume(30);
	}
	Character(sf::Sprite _head , sf::Sprite _body)
	{
		head = _head;
		body = _body;
	}
	void Death_Animation(sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Living_Object*> player, vector<Sound_Wav> &wav_db)
	{
		if ( ddown == true )
		{
			posy = 0;
			xhead = 35;
			posx = 15;
			body_texture.loadFromFile("character/swordsman/1DEAD1.png");
			head_texture.loadFromFile("character/swordsman/1HDEAD1.png");
		}
		if ( dup == true )
		{
			posy = 25;
			xhead = 30;
			posx = 5;
			body_texture.loadFromFile("character/swordsman/1DEAD5.png");
			head_texture.loadFromFile("character/swordsman/1HDEAD5.png");
		}
		if ( dleft == true )
		{
			posy = 30;
			xhead = 32.2;
			posx = 12.5;
			body_texture.loadFromFile("character/swordsman/1DEAD3.png");
			head_texture.loadFromFile("character/swordsman/1HDEAD3.png");
		}
		if ( dright == true )
		{
			posy = 0;
			xhead = 32.4;
			posx = 1.5;
			body_texture.loadFromFile("character/swordsman/1DEAD7.png");
			head_texture.loadFromFile("character/swordsman/1HDEAD7.png");
		}
		body.setTexture(body_texture);
		body.setTextureRect(sf::IntRect(0,0,body_texture.getSize().x,body_texture.getSize().y));
		head.setTexture(head_texture);
		head.setTextureRect(sf::IntRect(0,0,head_texture.getSize().x,head_texture.getSize().y));
		head.setPosition(body.getPosition().x + posx , body.getPosition().y + posy);
		alive = false;
	}
	void Hurt_Animation(sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Living_Object*> player, vector<Sound_Wav> &wav_db)
	{
		if ( delay_hurt > 0 )
		{
			delay_hurt --;
		}
		else
		{
			hurt_img_index ++;
			delay_hurt = delay_hurt_ori;
		}

		if(hp <= 0)
		{
			hurt_sound.play();
			check_atked = false;
			Death_Animation(window,monster,sprite_object,player,wav_db);
		}
		else
		{
			posy = -17;
			if ( ddown == true )
			{
				xhead = 35;
				posx = 5;
				body_texture.loadFromFile("character/swordsman/1HURT1.png");
				head_texture.loadFromFile("character/swordsman/1HHURT1.png");
			}
			if ( dup == true )
			{
				xhead = 30;
				posx = 5;
				body_texture.loadFromFile("character/swordsman/1HURT5.png");
				head_texture.loadFromFile("character/swordsman/1HHURT5.png");
			}
			if ( dleft == true )
			{
				xhead = 32.2;
				posx = 7.5;
				body_texture.loadFromFile("character/swordsman/1HURT3.png");
				head_texture.loadFromFile("character/swordsman/1HHURT3.png");
			}
			if ( dright == true )
			{
				xhead = 32.4;
				posx = 1.5;
				body_texture.loadFromFile("character/swordsman/1HURT7.png");
				head_texture.loadFromFile("character/swordsman/1HHURT7.png");
			}

			if( hurt_img_index == 0)
			{
				hurt_sound.play();
			}

			if ( hurt_img_index >= 3 )
			{
				hurt_img_index = 0;
				check_atked = false;
			}
			body.setTexture(body_texture);
			body.setTextureRect(sf::IntRect(hurt_img_index*body_texture.getSize().x/3,0,body_texture.getSize().x/3,body_texture.getSize().y));
			head.setTexture(head_texture);
			head.setTextureRect(sf::IntRect(hurt_img_index*head_texture.getSize().x/3,0,head_texture.getSize().x/3,head_texture.getSize().y));
			head.setPosition(body.getPosition().x + posx , body.getPosition().y + posy);
		}
	}
	void Atk_Animation(vector<Living_Object*> monster, vector<Sound_Wav> &wav_db)
	{
		if ( alive == true )
		{
			if ( delay_atk > 0 )
			{
				delay_atk--;
			}
			else
			{
				delay_atk = mdelay_atk;
				atk_img_index ++;
			}
			if ( ddown == true )
			{
				body_texture.loadFromFile("character/swordsman/1ATTACK1.png");
				head_texture.loadFromFile("character/swordsman/1HATTACK1.PNG");
				weapon_texture.loadFromFile("character/swordsman/1WATTACK1.png");
				weapon_aura_texture.loadFromFile("character/swordsman/1WATTACKA1.png");
				posx = -2.5;
				posy = -14.5;
				posx_w = -20;
				posy_w = 0;
				posx_wa = 0;
				posy_wa = -40;
			}
			if ( dup == true )
			{
				body_texture.loadFromFile("character/swordsman/1ATTACK5.png");
				head_texture.loadFromFile("character/swordsman/1HATTACK5.PNG");
				weapon_texture.loadFromFile("character/swordsman/1WATTACK5.png");
				weapon_aura_texture.loadFromFile("character/swordsman/1WATTACKA5.png");
				posx = -2.8;
				posy = -18.7;
				posx_w = -10;
				posy_w = -2;
				posx_wa = -6;
				posy_wa = -65;
			}
			if ( dleft == true )
			{
				body_texture.loadFromFile("character/swordsman/1ATTACK3.png");
				head_texture.loadFromFile("character/swordsman/1HATTACK3.PNG");
				weapon_texture.loadFromFile("character/swordsman/1WATTACK3.png");
				weapon_aura_texture.loadFromFile("character/swordsman/1WATTACKA3.png");
				posx = 4.5;
				posy = -18.7;
				posx_w = 30;
				posy_w = 0;
				posx_wa = -45;
				posy_wa = -65;
			}
			if (dright == true )
			{
				body_texture.loadFromFile("character/swordsman/1ATTACK7.png");
				head_texture.loadFromFile("character/swordsman/1HATTACK7.PNG");
				weapon_texture.loadFromFile("character/swordsman/1WATTACK7.png");
				weapon_aura_texture.loadFromFile("character/swordsman/1WATTACKA7.png");
				posx = 6;
				posy = -13.7;
				posx_w = -5;
				posy_w = 0;
				posx_wa = -25;
				posy_wa = -45;
				/*posx_wa = 0;
				posy_wa = -20;*/
			}
			if ( atk_img_index >= 8 )
			{
				atk_img_index = 0;
			}
			body.setTexture(body_texture);
			body.setTextureRect(sf::IntRect(atk_img_index*body_texture.getSize().x/9, 0 , body_texture.getSize().x/9 , body_texture.getSize().y));
			head.setTexture(head_texture);
			head.setTextureRect(sf::IntRect(atk_img_index*head_texture.getSize().x/9, 0 , head_texture.getSize().x/9 , head_texture.getSize().y));
			head.setPosition(body.getPosition().x + posx , body.getPosition().y + posy);
			weapon.setTexture(weapon_texture);
			weapon.setTextureRect(sf::IntRect(atk_img_index*weapon_texture.getSize().x/9, 0, weapon_texture.getSize().x/9, weapon_texture.getSize().y));
			weapon.setPosition(body.getPosition().x + posx_w, body.getPosition().y + posy_w);
			weapon_aura.setTexture(weapon_aura_texture);
			weapon_aura.setTextureRect(sf::IntRect(atk_img_index*weapon_aura_texture.getSize().x/9, 0, weapon_aura_texture.getSize().x/9, weapon_aura_texture.getSize().y));
			weapon_aura.setPosition(weapon.getPosition().x + posx_wa, weapon.getPosition().y + posy_wa);

			if ( atk_img_index == 3 )
			{
				attack_sound.play();
			}

			if ( atk_img_index == 6 )
			{
				for(int i = 0; i < monster.size(); i++)
				{
					if(Battle(weapon_aura,monster[i]->getWall()) == true)
					{
						if(monster[i]->getCheckAtked() == false && monster[i]->getDying() == false)
						{
							int damage;
							if(atk < monster[i]->getDef())
							{
								damage = 0;
							}
							else
							{
								damage = atk - monster[i]->getDef();
							}
							monster[i]->setCheckAtked(true);
							monster[i]->setHP(monster[i]->getHP()-damage);
							monster[i]->setAggro(true);
							attack_hit.play();
						}
					}
					else
					{
						monster[i]->setCheckAtked(false);
					}
				}
			}
		}
	}
	void Move_Animation(vector<sf::Sprite> wall_object , int index, vector<Living_Object*> player, sf::RenderWindow &window, vector<Living_Object*> monster, vector<Object> sprite_object, vector<Sound_Wav> &wav_db)
	{
		if( check_atked == false && alive == true )
		{
			if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				if ( delay > 0 )
				{
					delay --;
				}
				else
				{
					img_index ++;
					delay = delay_ori;
				}
				if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				{
					if ( check_left == false && check_down == false && check_up == false )
					{
						check_right = true;
					}
					if ( check_right == true )
					{
						dright = true;
						dleft = false;
						dup = false;
						ddown = false;
						if(check_atk == false)
						{
							body_texture.loadFromFile("character/swordsman/1MOVE6.png");
							head_texture.loadFromFile("character/swordsman/1HMOVE6.png");
						}
						posx = 7.5;
						posy = -17;
						x = 54;
						xhead = 32.4;
					}
					if ( check_left == false  )
					{
						body.setPosition(body.getPosition().x + speed,body.getPosition().y);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x - speed,body.getPosition().y);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 4 );
						}
					}
				}
				else
				{
					check_right = false;
				}
				if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				{
					if ( check_right == false && check_down == false && check_up == false )
					{
						check_left = true;
					}
					if ( check_left == true )
					{
						dright = false;
						dleft = true;
						dup = false;
						ddown = false;
						if(check_atk == false)
						{
							body_texture.loadFromFile("character/swordsman/1MOVE2.png");
							head_texture.loadFromFile("character/swordsman/1HMOVE2.png");
						}
						posx = 13.5;
						posy = -17;
						x = 52.2;
						xhead = 32.2;
					}
					if ( check_right == false )
					{					
						body.setPosition(body.getPosition().x - speed,body.getPosition().y);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x + speed,body.getPosition().y);
							wall.setPosition(body.getPosition().x + 4 , body.getPosition().y + 3 );
						}
					}
				}
				else
				{
					check_left = false;
				}
				if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				{
					if ( check_right == false && check_down == false && check_left == false )
					{
						check_up = true;
					}
					if ( check_up == true )
					{
						dright = false;
						dleft = false;
						dup = true;
						ddown = false;
						if(check_atk == false)
						{
							body_texture.loadFromFile("character/swordsman/1MOVE4.png");
							head_texture.loadFromFile("character/swordsman/1HMOVE4.png");
						}
						x = 44;
						xhead = 30;
						posx = 7;
						posy = -17;
					}
					if ( check_down == false )
					{				
						body.setPosition(body.getPosition().x,body.getPosition().y - speed);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x,body.getPosition().y + speed);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3);
						}
					}
				}
				else
				{
					check_up = false;
				}
				if( sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				{
					if ( check_right == false && check_left == false && check_up == false )
					{
						check_down = true;
					}
					if ( check_down == true )
					{
						dright = false;
						dleft = false;
						dup = false;
						ddown = true;
						if(check_atk == false)
						{
							body_texture.loadFromFile("character/swordsman/1MOVE0.png");
							head_texture.loadFromFile("character/swordsman/1HMOVE0.png");
						}
						x = 45;
						posx = 5;
						posy = -17;
						xhead = 35;
					}
					if ( check_up == false )
					{				
						body.setPosition(body.getPosition().x,body.getPosition().y + speed);
						wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						if ( CheckHitBox(wall,wall_object,index) == true )
						{
							body.setPosition(body.getPosition().x,body.getPosition().y - speed);
							wall.setPosition(body.getPosition().x + 5 , body.getPosition().y + 3 );
						}
					}
				}
				else
				{
					check_down = false;
				}
			}
			else
			{
				if ( check_atk == false)
				{
					posy = -17;
					if ( ddown == true )
					{
						xhead = 35;
						posx = 5;
						head_texture.loadFromFile("character/swordsman/1HMOVE0.png");
						body_texture.loadFromFile("character/swordsman/1IDLE0.png");
					}
					if ( dup == true )
					{
						xhead = 30;
						posx = 5;
						head_texture.loadFromFile("character/swordsman/1HMOVE4.png");
						body_texture.loadFromFile("character/swordsman/1IDLE4.png");
					}
					if ( dleft == true )
					{
						xhead = 32.2;
						posx = 7.5;
						head_texture.loadFromFile("character/swordsman/1HMOVE2.png");
						body_texture.loadFromFile("character/swordsman/1IDLE2.png");
					}
					if ( dright == true )
					{
						xhead = 32.4;
						posx = 1.5;
						head_texture.loadFromFile("character/swordsman/1HMOVE6.png");
						body_texture.loadFromFile("character/swordsman/1IDLE6.png");
					}
					img_index = 0;
				}
			}
			if ( img_index >= 7 )
			{
				img_index = 0;
			}
		}

		if ( alive == false )
		{
			Death_Animation(window,monster,sprite_object,player,wav_db);
		}
		else if ( check_atked == true)
		{
			Hurt_Animation(window,monster,sprite_object,player,wav_db);
		}
		else if ( check_atk == true )
		{
			Atk_Animation(monster,wav_db);
		}
		else
		{
			body.setTexture(body_texture);
			body.setTextureRect(sf::IntRect(img_index*x,0,48,76));
			head.setTexture(head_texture);
			head.setPosition(body.getPosition().x + posx , body.getPosition().y + posy);
			head.setTextureRect(sf::IntRect(img_index*xhead,0,34,40));
		}
	}
};

void Exterminate_Monster(vector<Living_Object*> &monster)
{
	for(int i = 0; i < monster.size(); i++)
	{
		if(monster[i] -> getAlive() == false)
		{
			monster.erase(monster.begin()+i);
		}
	}
}

void Load_Monster(vector<Living_Object*> &monster, vector<Living_Object*> player, bool boss)
{
	fstream monster_db;
	int id, id_scan, idle_limit, move_limit, atk_limit, hurt_limit, death_limit, delay, delay_atk, delay_hurt, speed, hp, mp, atk, def, death_move_x, death_move_y, aggresive, exp;
	int death_speed, attack_range_x, attack_range_y;
	int death_roll;

	if(boss == true)
	{
		id = 6;
	}
	else if(player[0]->getLevel() > 4)
	{
		id = rand()%5+1;
	}
	else
	{
		id = rand()%2+player[0]->getLevel();
	}
	monster_db.open("monster_db.txt",ios_base::in);
	while(true)
	{
		monster_db >> id_scan;
		if(monster_db.eof())
		{
			break;
		}
		monster_db >> idle_limit;
		monster_db >> move_limit;
		monster_db >> atk_limit;
		monster_db >> hurt_limit;
		monster_db >> death_limit;
		monster_db >> delay;
		monster_db >> delay_atk;
		monster_db >> delay_hurt;
		monster_db >> speed;
		monster_db >> hp;
		monster_db >> mp;
		monster_db >> atk;
		monster_db >> def;
		monster_db >> death_move_x;
		monster_db >> death_move_y;
		monster_db >> aggresive;
		monster_db >> death_speed;
		monster_db >> exp;
		monster_db >> attack_range_x;
		monster_db >> attack_range_y;
		if(id == id_scan)
		{
			monster.push_back(new Living_Object(delay,delay_atk,speed,idle_limit,move_limit,atk_limit,hurt_limit,death_limit,delay_hurt,hp,mp,atk,def,id,aggresive,death_move_x,death_move_y,death_speed,exp,attack_range_x,attack_range_y));
			break;
		}
	}
}

void main()
{
	bool boss = false;
	int choice = 1;
	bool start_page = true;
	bool reload = false;
	bool status_window;
	status_window = false;
	sf::Texture status_window_texture;
	status_window_texture.loadFromFile("chicken.jpg");
	sf::RectangleShape status_window_bar(sf::Vector2f(600,500));
	status_window_bar.setPosition(300,100);
	status_window_bar.setTexture(&status_window_texture);
	sf::RectangleShape atk_up_button(sf::Vector2f(100,50));
	sf::RectangleShape hp_up_button(sf::Vector2f(100,50));
	sf::RectangleShape def_up_button(sf::Vector2f(100,50));
	sf::RectangleShape mp_up_button(sf::Vector2f(100,50));
	sf::RectangleShape save_button(sf::Vector2f(100,50));
	sf::RectangleShape load_button(sf::Vector2f(100,50));

	hp_up_button.setPosition(350,250);
	mp_up_button.setPosition(350,350);
	atk_up_button.setPosition(500,250);
	def_up_button.setPosition(500,350);
	save_button.setPosition(350,486);
	load_button.setPosition(500,486);

	hp_up_button.setOutlineColor(sf::Color(0,0,0,255));
	hp_up_button.setOutlineThickness(0);
	mp_up_button.setOutlineColor(sf::Color(0,0,0,255));
	mp_up_button.setOutlineThickness(0);
	atk_up_button.setOutlineColor(sf::Color(0,0,0,255));
	atk_up_button.setOutlineThickness(0);
	def_up_button.setOutlineColor(sf::Color(0,0,0,255));
	def_up_button.setOutlineThickness(0);

	srand(time(0));
	sf::RenderWindow window(sf::VideoMode(1366,768), "Xelda", sf::Style::Fullscreen); // buat window
	int tile;
	ifstream infile;
	infile.open("arena.txt");
	sf::Texture texture_object;
	sf::Texture texture_tile[10];
	texture_tile[0].loadFromFile("tile/jt_spl010.bmp");
	//texture_tile[1].loadFromFile("tile/straight_horizontal.bmp");
	//texture_tile[2].loadFromFile("tile/straight_vertical.bmp");
	//texture_tile[3].loadFromFile("tile/curve1.bmp");
	//texture_tile[4].loadFromFile("tile/curve2.bmp");
	//texture_tile[5].loadFromFile("tile/curve3.bmp");
	//texture_tile[6].loadFromFile("tile/curve4.bmp");
	sf::RectangleShape tile_single(sf::Vector2f(256,256));
	vector <sf::RectangleShape> tiles;
	vector <Object>sprite_object(1000);
	vector <sf::Sprite>wall_object(1000);
	int index = 0;
	sf::Vector2i map_length;
	double j = 0.0;
	if(infile.is_open())
	{
		infile >> map_length.x;
		infile >> map_length.y;
		for ( int k = 0 ; k < map_length.y ; k++)
		{
			for ( int i = 0 ; i < map_length.x ; i++)
			{
				infile >> tile;
				if ( tile == 1 )
				{
					/*texture_object.loadFromFile("boulder.png",sf::IntRect(0,0,32,32));*/
					wall_object[index].setTextureRect(sf::IntRect(0,0,1,1));
					wall_object[index].setPosition(i * 32 + 17 , j);
					sprite_object[index] = Object( texture_object , wall_object[index] );
					sprite_object[index].setPosition(i * 32 , j);
					index++;
				}
				else if ( tile == 2 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 3 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 4 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 5 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 6 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 7 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
				else if ( tile == 8 )
				{
					tile_single.setTexture(&texture_tile[0]);
					tile_single.setPosition(i * 32, j);
					tiles.push_back(tile_single);
				}
			}
			j += 30;
		}
	}	
	infile.close();
	sf::Texture texture;
	sf::Sprite char_sprite;
	sf::Font pixel_font;
	sf::Text hp_text, mp_text, level_text, exp_text, hp_up_text, mp_up_text, atk_up_text, def_up_text, atk_text, def_text, skill_point_text, save_text, load_text;
	ostringstream convert_hp, convert_maxhp, convert_mp, convert_maxmp, convert_level, convert_exp, convert_maxexp, convert_atk, convert_def, convert_skill_point;
	
	pixel_font.loadFromFile("LCD_Solid.ttf");

	save_text.setFont(pixel_font);
	save_text.setCharacterSize(32);
	save_text.setPosition(355,495);
	save_text.setColor(sf::Color(0,0,0,255));
	save_text.setString("SAVE");

	load_text.setFont(pixel_font);
	load_text.setCharacterSize(32);
	load_text.setPosition(505,495);
	load_text.setColor(sf::Color(0,0,0,255));
	load_text.setString("LOAD");

	skill_point_text.setFont(pixel_font);
	skill_point_text.setCharacterSize(32);
	skill_point_text.setPosition(350, 427);
	skill_point_text.setColor(sf::Color(0,0,0,255));

	hp_up_text.setFont(pixel_font);
	hp_up_text.setCharacterSize(32);
	hp_up_text.setPosition(355,259);
	hp_up_text.setColor(sf::Color(0,0,0,255));
	hp_up_text.setString("HP+");

	mp_up_text.setFont(pixel_font);
	mp_up_text.setCharacterSize(32);
	mp_up_text.setPosition(355,359);
	mp_up_text.setColor(sf::Color(0,0,0,255));
	mp_up_text.setString("MP+");

	atk_up_text.setFont(pixel_font);
	atk_up_text.setCharacterSize(32);
	atk_up_text.setPosition(505,259);
	atk_up_text.setColor(sf::Color(0,0,0,255));
	atk_up_text.setString("ATK+");

	atk_text.setFont(pixel_font);
	atk_text.setCharacterSize(32);
	atk_text.setPosition(350,127);
	atk_text.setColor(sf::Color(0,0,0,255));

	def_up_text.setFont(pixel_font);
	def_up_text.setCharacterSize(32);
	def_up_text.setPosition(505,359);
	def_up_text.setColor(sf::Color(0,0,0,255));
	def_up_text.setString("DEF+");

	def_text.setFont(pixel_font);
	def_text.setCharacterSize(32);
	def_text.setPosition(350,191);
	def_text.setColor(sf::Color(0,0,0,255));

	hp_text.setFont(pixel_font);
	hp_text.setCharacterSize(32);
	hp_text.setPosition(32,32);
	hp_text.setColor(sf::Color(255,255,255,255));

	mp_text.setFont(pixel_font);
	mp_text.setCharacterSize(32);
	mp_text.setPosition(32,96);
	mp_text.setColor(sf::Color(255,255,255,255));

	level_text.setFont(pixel_font);
	level_text.setCharacterSize(32);
	level_text.setPosition(32,672);
	level_text.setColor(sf::Color(255,255,255,255));

	exp_text.setFont(pixel_font);
	exp_text.setCharacterSize(32);
	exp_text.setPosition(32,736);
	exp_text.setColor(sf::Color(255,255,255,255));


	vector<Living_Object*> player;
	player.push_back (new Character());
	vector<Living_Object*> monster;
	vector<Sound_Wav> wav_db;
	ostringstream convert_i;
	Sound_Wav temp_wav[6];
	int wav_counter = 0;
	for(int i = 1; i <= 6; i++)
	{
		convert_i.str("");
		convert_i << i;
		temp_wav[wav_counter].Set_Wav("monster/"+convert_i.str()+"MMOVEM.wav","monster/"+convert_i.str()+"MATTACKM.wav","monster/"+convert_i.str()+"MHURTM.wav","monster/"+convert_i.str()+"MDEADM.wav");
		wav_db.push_back(temp_wav[wav_counter]);
		wav_counter++;
	}

	texture.loadFromFile("character/swordsman/1MOVE0.png");
	char_sprite.setTexture(texture);
	char_sprite.setTextureRect(sf::IntRect(0,0,texture.getSize().x/8,texture.getSize().y));
	char_sprite.setPosition(300.0,300.0);
	player[0] -> setBody(char_sprite);
	player[0] -> setBodyTexture(texture);

	texture.loadFromFile("character/swordsman/1HMOVE0.png");
	char_sprite.setTexture(texture);
	char_sprite.setTextureRect(sf::IntRect(0,0,32,64));
	player[0] -> setHead(char_sprite);
	player[0] -> setHeadTexture(texture);

	player[0] -> Update_MExp();

	/*texture.loadFromFile("monster/1MIDLE1.png");
	char_sprite.setTexture(texture);
	char_sprite.setTextureRect(sf::IntRect(0,0,texture.getSize().x/4,texture.getSize().y));
	char_sprite.setPosition(400.0,400.0);
	monster[0] -> setBody(char_sprite);
	monster[0] -> setBodyTexture(texture);*/
	/*window.setFramerateLimit(360);*/
	window.setVerticalSyncEnabled(true);

	sf::Texture background_texture, logo_texture, sword_pointer_texture;
	sf::RectangleShape background_image(sf::Vector2f(1366,768));
	sf::RectangleShape logo(sf::Vector2f(128,128));
	sf::RectangleShape sword_pointer(sf::Vector2f(240,240));

	sword_pointer_texture.loadFromFile("sword_pointer.png");
	sword_pointer.setTexture(&sword_pointer_texture);
	/*sword_pointer.setPosition(420,525);*/

	logo_texture.loadFromFile("zelda_logo.png");
	logo.setTexture(&logo_texture);
	logo.setPosition(520,90);

	background_texture.loadFromFile("forest_background.jpg");
	background_image.setTexture(&background_texture);
	background_image.setPosition(0,0);
	
	sf::Font title1_font, title2_font, title3_font;
	title1_font.loadFromFile("CharlemagneBold.otf");
	title2_font.loadFromFile("Triforce.ttf");
	title3_font.loadFromFile("SherwoodRegular.ttf");

	sf::Text new_game_text, load_game_text, title1_text, title2_text, title3_text;
	
	new_game_text.setFont(pixel_font);
	load_game_text.setFont(pixel_font);
	
	new_game_text.setCharacterSize(32);
	new_game_text.setColor(sf::Color(255,255,255,255));
	new_game_text.setString("New Game");
	new_game_text.setPosition(580,620);

	load_game_text.setCharacterSize(32);
	load_game_text.setColor(sf::Color(255,255,255,255));
	load_game_text.setString("Load Game");
	load_game_text.setPosition(583, 684);

	title1_text.setFont(title1_font);
	title2_text.setFont(title2_font);
	title3_text.setFont(title3_font);

	title1_text.setCharacterSize(32);
	title1_text.setColor(sf::Color(255,255,255,255));
	title1_text.setString("The Legend Of ");
	title1_text.setPosition(520,60);

	title2_text.setCharacterSize(64);
	title2_text.setColor(sf::Color(255,255,255,255));
	title2_text.setString("Xolda ");
	title2_text.setPosition(660,110);

	title3_text.setCharacterSize(32);
	title3_text.setColor(sf::Color(255,255,255,255));
	title3_text.setString("Mistery of the Endless Forest");
	title3_text.setPosition(440,210);

	sf::SoundBuffer option_buffer;
	sf::Sound option_sound;

	option_buffer.loadFromFile("pick_option.wav");
	option_sound.setBuffer(option_buffer);
	option_sound.setVolume(30);

	sf::Music bg_music;

	bg_music.openFromFile("opening.ogg");
	bg_music.setVolume(50);
	bg_music.setLoop(true);
	bg_music.play();

	/*opening_bg_buffer.loadFromFile("opening.ogg");
	opening_bg_sound.setBuffer(opening_bg_buffer);
	opening_bg_sound.setVolume(50);
	opening_bg_sound.setLoop(true);
	opening_bg_sound.play();*/
	bool loading = false;
	while ( start_page == true )
	{
		sf::Event coba_event;
		while(window.pollEvent(coba_event))
		{
			if(coba_event.type == sf::Event::KeyReleased)
			{
				if(coba_event.key.code == sf::Keyboard::Return)
				{
					option_sound.play();
					if(choice == 2)
					{
						int value;
						fstream load_file;
						load_file.open("player_db.txt",ios_base::in);
						load_file >> value;
						player[0] -> setLevel(value);
						player[0] -> Update_MExp();
						load_file >> value;
						player[0] -> Update_Exp(value);
						load_file >> value;
						player[0] -> setSkillPoint(value);
						load_file >> value;
						player[0] -> setHP(value);
						load_file >> value;
						player[0] -> setMHP(value);
						load_file >> value;
						player[0] -> setMP(value);
						load_file >> value;
						player[0] -> setMMP(value);
						load_file >> value;
						player[0] -> setAtk(value);
						load_file >> value;
						player[0] -> setDef(value);
						load_file.close();
					}
					start_page = false;
					loading = true;
				}
				else if(coba_event.key.code == sf::Keyboard::Escape)
				{
					start_page = false;
					window.close();
				}
				else if(coba_event.key.code == sf::Keyboard::Down)
				{
					option_sound.play();
					if(choice < 2)
					{
						choice ++;
					}
					else
					{
						choice = 1;
					}
				}
				else if(coba_event.key.code == sf::Keyboard::Up)
				{
					option_sound.play();
					if(choice > 1)
					{
						choice --;
					}
					else
					{
						choice = 2;
					}
				}
			}
		}
		if(choice == 1)
		{
			sword_pointer.setPosition(420,520);
		}
		else if(choice == 2)
		{
			sword_pointer.setPosition(420,585);
		}
		
		window.draw(background_image);
		window.draw(new_game_text);
		window.draw(load_game_text);
		window.draw(title1_text);
		window.draw(title2_text);
		window.draw(title3_text);
		window.draw(logo);
		window.draw(sword_pointer);
		window.display();
		window.clear();
	}
	bg_music.stop();

	for(int i = 0; i < 5; i++)
	{
		Load_Monster(monster,player,boss);
	}

	for(int i = 0; i < monster.size(); i++)
	{
		int map_x, map_y;
		map_x = rand()%800+200;
		map_y = rand()%400+100;
		ostringstream convert_id;
		string file_name;
		file_name.clear();
		convert_id.str("");
		convert_id << monster[i] ->getID();

		file_name = "monster/";
		file_name += convert_id.str();
		file_name = file_name + "MIDLE1.png";

		texture.loadFromFile(file_name);
		char_sprite.setTextureRect(sf::IntRect(0,0,texture.getSize().x/monster[i]->getIdleLimit(),texture.getSize().y));
		char_sprite.setPosition(map_x,map_y);
		monster[i] -> setBody(char_sprite);
		monster[i] -> setBodyTexture(texture);
	}


	bg_music.openFromFile("forest.ogg");
	bg_music.play();
	while ( window.isOpen() )
	{
		if(monster.size() == 0 || reload == true)
		{
			if(player[0]->getLevel() >= 10)
			{
				int boss_random;
				boss_random = rand()%100+1;
				if(boss_random <= 20)
				{
					boss = true;
				}
				else
				{
					boss = false;
				}
			}

			if(boss == false)
			{
				for(int i = 0; i < 5; i++)
				{
					Load_Monster(monster,player,boss);
				}

				for(int i = 0; i < monster.size(); i++)
				{
					int map_x, map_y;
					map_x = rand()%800+200;
					map_y = rand()%400+100;
					ostringstream convert_id;
					string file_name;
					file_name.clear();
					convert_id.str("");
					convert_id << monster[i] ->getID();

					file_name = "monster/";
					file_name += convert_id.str();
					file_name = file_name + "MIDLE1.png";

					texture.loadFromFile(file_name);
					char_sprite.setTextureRect(sf::IntRect(0,0,texture.getSize().x/monster[i]->getIdleLimit(),texture.getSize().y));
					char_sprite.setPosition(map_x,map_y);
					monster[i] -> setBody(char_sprite);
					monster[i] -> setBodyTexture(texture);
				}
			}
			else
			{
				Load_Monster(monster,player,boss);
				int map_x, map_y;
				map_x = rand()%800+200;
				map_y = rand()%400+100;
				ostringstream convert_id;
				string file_name;
				file_name.clear();
				convert_id.str("");
				convert_id << monster[0] ->getID();

				file_name = "monster/";
				file_name += convert_id.str();
				file_name = file_name + "MIDLE1.png";

				texture.loadFromFile(file_name);
				char_sprite.setTextureRect(sf::IntRect(0,0,texture.getSize().x/monster[0]->getIdleLimit(),texture.getSize().y));
				char_sprite.setPosition(map_x,map_y);
				monster[0] -> setBody(char_sprite);
				monster[0] -> setBodyTexture(texture);
			}
			reload = false;
		}

		sf::Event event_coba; 
		while ( window.pollEvent(event_coba) ) 
		{
			if ( event_coba.type == sf::Event::Closed )
			{
				window.close();
			}

			if ( event_coba.type == sf::Event::KeyReleased)
			{
				if(event_coba.key.code == sf::Keyboard::Escape)
				{
					window.close();
				}
				if(event_coba.key.code == sf::Keyboard::K)
				{
					if(status_window == false)
					{
						status_window = true;
					}
					else
					{
						status_window = false;
					}
				}
				if(event_coba.key.code == sf::Keyboard::R)
				{
					if(player[0]->getAlive() == false)
					{
						player[0]->setAlive(true);
						player[0]->setCheckAtked(false);
						player[0]->setHP(player[0]->getMHP());
						player[0]->Update_Exp((-1) * player[0]->getMExp() / 10);
						monster.clear();
						reload = true;
					}
				}
			}

			if ( event_coba.type == sf::Event::MouseButtonReleased)
			{
				if ( event_coba.mouseButton.button == sf::Mouse::Left )
				{
					if(status_window == true)
					{
						if (hp_up_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							player[0] -> hpUP();
						}
						else if (mp_up_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							player[0] -> mpUP();
						}
						else if (atk_up_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							player[0] -> atkUP();
						}
						else if (def_up_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							player[0] -> defUP();
						}
						else if (save_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							fstream save_file;
							save_file.open("player_db.txt",ios_base::out);
							save_file << player[0] -> getLevel() << " ";
							save_file << player[0] -> getExp() << " ";
							save_file << player[0] -> getSkillPoint() << " ";
							save_file << player[0] -> getHP() << " ";
							save_file << player[0] -> getMHP() << " ";
							save_file << player[0] -> getMP() << " ";
							save_file << player[0] -> getMMP() << " ";
							save_file << player[0] -> getAtk() << " ";
							save_file << player[0] -> getDef() << " ";
							save_file.close();
						}
						else if (load_button.getGlobalBounds().contains(window.mapPixelToCoords(sf::Mouse::getPosition(window))))
						{
							int value;
							fstream load_file;
							load_file.open("player_db.txt",ios_base::in);
							load_file >> value;
							player[0] -> setLevel(value);
							player[0] -> Update_MExp();
							load_file >> value;
							player[0] -> Update_Exp(value);
							load_file >> value;
							player[0] -> setSkillPoint(value);
							load_file >> value;
							player[0] -> setHP(value);
							load_file >> value;
							player[0] -> setMHP(value);
							load_file >> value;
							player[0] -> setMP(value);
							load_file >> value;
							player[0] -> setMMP(value);
							load_file >> value;
							player[0] -> setAtk(value);
							load_file >> value;
							player[0] -> setDef(value);
							load_file.close();
						}
					}
				}
			}
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			player[0] -> setCheckAtk(true);
		}
		else
		{
			player[0] -> setCheckAtk(false);
		}

		player[0] -> Move_Animation(wall_object , index, player, window, monster, sprite_object,wav_db);
		for(int j = 0; j < 1; j++)
		{
			for(int i = 0; i < monster.size(); i++)
			{
				if(monster[i] -> getAlive() == true)
				{
					monster[i] -> Random_Movement();
					monster[i] -> Move_Animation(wall_object , index, player, window, monster, sprite_object, wav_db);
				}
			}
		}

		Exterminate_Monster(monster);

		convert_hp.str("");
		convert_maxhp.str("");
		convert_mp.str("");
		convert_maxmp.str("");
		convert_hp << player[0] -> getHP();
		convert_maxhp << player[0] -> getMHP();
		convert_mp << player[0] -> getMP();
		convert_maxmp << player[0] -> getMMP();

		hp_text.setString("HP : " + convert_hp.str() + " / " + convert_maxhp.str());
		mp_text.setString("MP : " + convert_mp.str() + " / " + convert_maxmp.str());

		convert_level.str("");
		convert_exp.str("");
		convert_maxexp.str("");
		convert_level << player[0] -> getLevel();
		convert_exp << player[0] -> getExp();
		convert_maxexp << player[0] -> getMExp();

		level_text.setString("Level : " + convert_level.str());
		exp_text.setString("Exp : " + convert_exp.str() + " / " + convert_maxexp.str());

		convert_atk.str("");
		convert_def.str("");
		convert_atk << player[0] -> getAtk();
		convert_def << player[0] -> getDef();

		atk_text.setString("ATK : " + convert_atk.str());
		def_text.setString("DEF : " + convert_def.str());

		convert_skill_point.str("");
		convert_skill_point << player[0] -> getSkillPoint();

		skill_point_text.setString("Skill Point : " + convert_skill_point.str());

		for ( int i = 0 ; i < tiles.size() ; i ++)
		{
			window.draw(tiles[i]);
		}

		/*for ( int i = 0 ; i < index ; i ++)
		{
			window.draw(sprite_object[i]);
		}*/
		
		window.draw(player[0] -> getBody());
		window.draw(player[0] -> getHead());
		for(int i = 0; i < monster.size(); i++)
		{
			if(monster[i] -> getAlive() == true)
			{
				window.draw(monster[i] -> getBody());
			}
		}
		if(player[0] -> getCheckAtk() == true)
		{
			window.draw(player[0] -> getWeaponAura());
			window.draw(player[0] -> getWeapon());
		}
		window.draw(hp_text);
		window.draw(mp_text);
		window.draw(level_text);
		window.draw(exp_text);
		if(status_window == true)
		{
			window.draw(status_window_bar);
			window.draw(hp_up_button);
			window.draw(mp_up_button);
			window.draw(atk_up_button);
			window.draw(def_up_button);
			window.draw(hp_up_text);
			window.draw(mp_up_text);
			window.draw(atk_up_text);
			window.draw(def_up_text);
			window.draw(atk_text);
			window.draw(def_text);
			window.draw(skill_point_text);
			window.draw(save_button);
			window.draw(load_button);
			window.draw(save_text);
			window.draw(load_text);
		}
		window.display();
		window.clear(); 
	}
	bg_music.stop();
}


//Z is pressed
/*for ( int j = 0 ; j < index ; j++)
{
	window.draw(sprite_object[j]);
}*/
/*window.draw(livingobjects[0] -> getBody());
window.draw(livingobjects[0] -> getHead());
window.draw(livingobjects[0] -> getWeapon());
				
window.display();
window.clear();*/